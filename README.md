Sample WordNet glosses annotated with semantic roles for definitions.

The glosses were automatically annotated by a rule-based system based on the 
syntactic patterns described in the following paper:

> Vivian S. Silva, Siegfried Handschuh and André Freitas. Categorization of 
Semantic Roles for Dictionary Definitions. Cognitive Aspects of the Lexicon 
(CogALex-V), Workshop at the 26th International Conference on Computational 
Linguistics (COLING), Osaka, 2016.

After the initial automatic annotation, the data was manually curated, using 
the annotation tool Brat (http://brat.nlplab.org/).

Ther are four files:

1) Synsets.txt: list of the randomly chosen synsets, in the format:

```    
id;pos;word_list;gloss
```
    
where:
- id: the synset's internal identifier
- pos: POS tag, "n" for noun synsets and "v" for verb synsets
- word_list: comma separeted list of the words in the synset
- gloss: the synset's description
    
2) Glosses.txt: a simple list of the glosses to be read by the Brat tool
    
3) Glosses.ann: the annotated data in the standoff format defined by the Brat 
tool. More details about the format can be found at 
http://brat.nlplab.org/standoff.html
    
4) AnnotatedGlosses.pdf: the annotated data as seen in the Brat tool graphical 
interface